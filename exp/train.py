import utils
from utils import get_mnist_data_loaders
from utils import progress
import os
import sys
import shutil
import copy
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from attacks import IterativeGradientAttack, ProjectedGradientDescent
N_TRAIN = 50000
N_VAL = 10000
N_TEST = 10000

CONFIG_FILE = './configs/config.json'
MODEL_SAVE_FILENAME = 'model{}_epoch_{}.pt'
EVAL_MODEL_FILENAME = 'model{}.pt'
LOSS_SAVE_FILENAME = 'loss_model{}.csv'
ACC_SAVE_FILENAME = 'acc_model{}.csv'
def update_learning_rate(optimizer, old_lr, new_lr):
    for param_group in optimizer.param_groups:
        param_group['lr'] = new_lr
"""
def progress(curr, total, suffix=''):
    bar_len = 48
    filled = int(round(bar_len * curr / float(total)))
    bar = '='*(filled - 1) + '>' + '-'*(bar_len - filled)
    sys.stdout.write('\r[%s] .. %s' % (bar, suffix))
    sys.stdout.flush()
"""
def normal(train_model, name, epochs, lr, batch_size, savedir):
    """
    Parameters
    -----------
    :train_model - instance of the model you want to train
    :name - name of the model (will be appended to log and model files)
    :epochs - number of epochs
    :lr - learning rate
    :batch_size - batch size for training
    :savedir - saving directory
    """
    DIR_PREFIX = ('/normal/model_' + name + '/').format(name)
    LOSS_SAVE_PATH = savedir + DIR_PREFIX + LOSS_SAVE_FILENAME
    MODEL_SAVE_PATH = savedir + DIR_PREFIX + MODEL_SAVE_FILENAME
    ACC_SAVE_PATH = savedir + DIR_PREFIX + ACC_SAVE_FILENAME
    LOSS_SAVE_PATH = LOSS_SAVE_PATH.format(name)
    ACC_SAVE_PATH = ACC_SAVE_PATH.format(name)
    EVAL_MODEL_PATH = (savedir + DIR_PREFIX + EVAL_MODEL_FILENAME).format(name)

    if not os.path.exists(savedir + DIR_PREFIX):
        os.makedirs(savedir + DIR_PREFIX)
    # Clearing out the contents of file
    f = open(LOSS_SAVE_PATH, 'w')
    f.truncate(0)
    f.close()
    f = open(ACC_SAVE_PATH, 'w')
    f.truncate(0)
    f.close()

    train_loader, val_loader, test_loader = get_mnist_data_loaders(batch_size, \
        N_TRAIN, N_VAL, N_TEST)
    print 'Data loaders initialized'
    model = train_model.to('cuda')
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    step = 0
    for epoch in range(epochs):
        for i, (images, labels) in enumerate(train_loader):
            images, labels = images.to('cuda'), labels.to('cuda')

            optimizer.zero_grad()

            outputs = model(images)

            loss = criterion(outputs, labels)
            loss.backward()

            optimizer.step()
            step += 1
            if i % 128 == 0:
                suffix = 'Epoch [{}/{}] Loss {:.3f}'\
                    .format(epoch+1, epochs, loss.item())
                #print 'Epoch [{}/{}], Batch [{},{}], Loss {}'\
                #    .format(epoch, epochs, i, len(train_loader), loss.item())
                progress(i, len(train_loader), suffix)
                with open(LOSS_SAVE_PATH, 'a') as lossfile:
                    lossfile.write('{},{}\n'.format(step, loss.item()))
        model.eval()
        torch.save(model.state_dict(), MODEL_SAVE_PATH.format(name, epoch))
        model.train()
        """
        model.eval()
        correct = 0
        for i, (images, labels) in enumerate(val_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
        model.train()
        # Length of val_loader is the number of BATCHES in the loader 
        # not the number of elements
        print 'Validation Accuracy = ' + str(float(correct)/N_VAL)
        with open(ACC_SAVE_PATH, 'a') as accfile:
            accfile.write('{},{}\n'.format(epoch, float(correct)/N_VAL))
        torch.save(model.state_dict(), MODEL_SAVE_PATH.format(name, epoch))
        """
        if (epoch+1) % int(epochs/4) == 0:
            print '\nDecreasing the learning rate to ' + str(lr/5)
            update_learning_rate(optimizer, lr, lr/5)
            lr = lr/5
            print 64*'-'

    print('\nTraining complete')
    model.eval()
    max_acc = {'acc': 0.0, 'epoch': -1}
    for epoch in range(epochs):
        model.load_state_dict(torch.load(MODEL_SAVE_PATH.format(name, epoch)))
        #model.eval()
        correct = 0
        for _, (images, labels) in enumerate(val_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
        acc = float(correct)/N_VAL
        print 'Epoch {} Validation Accuracy {}'.format(epoch, acc)
        if acc > max_acc['acc']:
            max_acc['acc'] = acc
            max_acc['epoch'] = epoch
        with open(ACC_SAVE_PATH, 'a') as accwriter:
            accwriter.write('{},{}\n'.format(epoch, acc))
    print('Validation complete')
    print('Maximum accuracy of {} observed at epoch {}'.format(max_acc['acc'], max_acc['epoch']))
    print('Keeping only epoch {}'.format(max_acc['epoch']))
    for epoch in range(epochs):
        if(epoch == max_acc['epoch']):
            shutil.move(MODEL_SAVE_PATH.format(name, epoch), EVAL_MODEL_PATH)
        else:
            os.remove(MODEL_SAVE_PATH.format(name, epoch))

def fgsm_adv(train_model, name, epochs, lr, batch_size, savedir):
    """
    Parameters
    -----------
    :train_model - instance of the model you want to train
    :name - name of the model (will be appended to log and model files)
    :epochs - number of epochs
    :lr - learning rate
    :batch_size - batch size for training
    :savedir - saving directory
    """
    DIR_PREFIX = ('/fgsm_adv/model_' + name + '/').format(name)
    LOSS_SAVE_PATH = savedir + DIR_PREFIX + LOSS_SAVE_FILENAME
    MODEL_SAVE_PATH = savedir + DIR_PREFIX + MODEL_SAVE_FILENAME
    ACC_SAVE_PATH = savedir + DIR_PREFIX + ACC_SAVE_FILENAME
    LOSS_SAVE_PATH = LOSS_SAVE_PATH.format(name)
    ACC_SAVE_PATH = ACC_SAVE_PATH.format(name)
    EVAL_MODEL_PATH = (savedir + DIR_PREFIX + EVAL_MODEL_FILENAME).format(name)

    if not os.path.exists(savedir + DIR_PREFIX):
        os.makedirs(savedir + DIR_PREFIX)
    # Clearing out the contents of file
    f = open(LOSS_SAVE_PATH, 'w')
    f.truncate(0)
    f.close()
    f = open(ACC_SAVE_PATH, 'w')
    f.truncate(0)
    f.close()

    train_loader, val_loader, test_loader = get_mnist_data_loaders(batch_size, \
        N_TRAIN, N_VAL, N_TEST)
    print 'Data loaders initialized'
    model = train_model.to('cuda')
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    config = utils.get_config_from_file(CONFIG_FILE)['train']['fgsm_params']
    eps_adv, eps_max, clip_min, clip_max = config['eps'], config['eps_max'], config['clip_min'], config['clip_max']
    step = 0
    adv_loss_fn = nn.CrossEntropyLoss() # does not have `size_average=False` for our own implementation
    for epoch in range(epochs):
        for i, (images, labels) in enumerate(train_loader):
            (img_batch_0, img_batch_1) = torch.split(images, int(len(images)/2), dim=0)
            (lbl_batch_0, lbl_batch_1) = torch.split(labels, int(len(images)/2), dim=0)
            model_adv = copy.deepcopy(model)
            for p in model_adv.parameters():
                p.requires_grad = False
            model_adv.eval()

            if eps_adv == -1:
                eps = np.abs(utils.truncated_normal(0, eps_max/2, len(img_batch_1)))[:, np.newaxis, np.newaxis, np.newaxis]

            adversary = IterativeGradientAttack(model_adv, adv_loss_fn, steps=1, clip_min=clip_min, clip_max=clip_max, eps=eps)
            # Replace half the batch with adversarial samples
            img_batch_1 = adversary(img_batch_1, lbl_batch_1)
            img_batch_1, lbl_batch_1 = img_batch_1.to('cuda'), lbl_batch_1.to('cuda')
            optimizer.zero_grad()

            outputs = model(img_batch_1)

            loss_adv = criterion(outputs, lbl_batch_1)
            loss_adv.backward()

            optimizer.step()

            optimizer.zero_grad()
            
            img_batch_0, lbl_batch_0 = img_batch_0.to('cuda'), lbl_batch_0.to('cuda')
            outputs = model(img_batch_0)
            loss_clean = criterion(outputs, lbl_batch_0)
            loss_clean.backward()
            optimizer.step()

            step += 1
            del model_adv # Free the memory 
            del adversary
            if i % 128 == 0:
                suffix = 'Epoch [{}/{}] loss_adv {:.3f} loss_clean {:.3f}'\
                    .format(epoch+1, epochs, loss_adv.item(), loss_clean.item())
                #print 'Epoch [{}/{}], Batch [{},{}], Loss {}'\
                #    .format(epoch, epochs, i, len(train_loader), loss.item())
                progress(i, len(train_loader), suffix)
                with open(LOSS_SAVE_PATH, 'a') as lossfile:
                    lossfile.write('{},{},{},{}\n'.format(step, loss_clean.item(), loss_adv.item(), eps))
        model.eval()
        torch.save(model.state_dict(), MODEL_SAVE_PATH.format(name, epoch))
        model.train()
        """
        model.eval()
        correct = 0
        for i, (images, labels) in enumerate(val_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
        model.train()
        # Length of val_loader is the number of BATCHES in the loader 
        # not the number of elements
        print 'Validation Accuracy = ' + str(float(correct)/N_VAL)
        with open(ACC_SAVE_PATH, 'a') as accfile:
            accfile.write('{},{}\n'.format(epoch, float(correct)/N_VAL))
        torch.save(model.state_dict(), MODEL_SAVE_PATH.format(name, epoch))
        """
        if (epoch+1) % int(epochs/4) == 0:
            print '\nDecreasing the learning rate to ' + str(lr/5)
            update_learning_rate(optimizer, lr, lr/5)
            lr = lr/5
            print 64*'-'

    print('\nTraining complete')
    model.eval()
    max_acc = {'acc': 0.0, 'epoch': -1}
    for epoch in range(epochs):
        model.load_state_dict(torch.load(MODEL_SAVE_PATH.format(name, epoch)))
        #model.eval()
        correct = 0
        for _, (images, labels) in enumerate(val_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
        acc = float(correct)/N_VAL
        print 'Epoch {} Validation Accuracy {}'.format(epoch, acc)
        if acc > max_acc['acc']:
            max_acc['acc'] = acc
            max_acc['epoch'] = epoch
        with open(ACC_SAVE_PATH, 'a') as accwriter:
            accwriter.write('{},{}\n'.format(epoch, acc))
    print('Validation complete')
    print('Maximum accuracy of {} observed at epoch {}'.format(max_acc['acc'], max_acc['epoch']))
    print('Keeping only epoch {}'.format(max_acc['epoch']))
    for epoch in range(epochs):
        if(epoch == max_acc['epoch']):
            shutil.move(MODEL_SAVE_PATH.format(name, epoch), EVAL_MODEL_PATH)
        else:
            os.remove(MODEL_SAVE_PATH.format(name, epoch))


def pgd_adv(train_model, name, epochs, lr, batch_size, savedir):
    """
    Parameters
    -----------
    :train_model - instance of the model you want to train
    :name - name of the model (will be appended to log and model files)
    :epochs - number of epochs
    :lr - learning rate
    :batch_size - batch size for training
    :savedir - saving directory
    """
    DIR_PREFIX = ('/pgd_adv/model_' + name + '/').format(name)
    LOSS_SAVE_PATH = savedir + DIR_PREFIX + LOSS_SAVE_FILENAME
    MODEL_SAVE_PATH = savedir + DIR_PREFIX + MODEL_SAVE_FILENAME
    ACC_SAVE_PATH = savedir + DIR_PREFIX + ACC_SAVE_FILENAME
    LOSS_SAVE_PATH = LOSS_SAVE_PATH.format(name)
    ACC_SAVE_PATH = ACC_SAVE_PATH.format(name)
    EVAL_MODEL_PATH = (savedir + DIR_PREFIX + EVAL_MODEL_FILENAME).format(name)

    if not os.path.exists(savedir + DIR_PREFIX):
        os.makedirs(savedir + DIR_PREFIX)
    # Clearing out the contents of file
    f = open(LOSS_SAVE_PATH, 'w')
    f.truncate(0)
    f.close()
    f = open(ACC_SAVE_PATH, 'w')
    f.truncate(0)
    f.close()

    train_loader, val_loader, test_loader = get_mnist_data_loaders(batch_size, \
        N_TRAIN, N_VAL, N_TEST)
    print 'Data loaders initialized'
    model = train_model.to('cuda')
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    step = 0
    adv_loss_fn = nn.CrossEntropyLoss() # does not need `size_average=False` for our implementation
    config = utils.get_config_from_file('./configs/config.json')['train']['pgd_params']
    eps, eps_iter, steps, clip_min, clip_max  = config['eps'], config['eps_iter'], config['steps'],\
        config['clip_min'],config['clip_max']
    for epoch in range(epochs):
        for i, (images, labels) in enumerate(train_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            model_adv = copy.deepcopy(model)
            for p in model_adv.parameters():
                p.requires_grad = False
            model_adv.eval()
            adversary = ProjectedGradientDescent(model_adv, adv_loss_fn,eps=eps,\
            eps_iter=eps_iter, steps=steps, clip_min=clip_min, clip_max=clip_max)
            # Replace entire batch with adversarial samples
            adv_images = adversary(images, labels)

            optimizer.zero_grad()

            outputs = model(adv_images)

            loss = criterion(outputs, labels)
            loss.backward()

            optimizer.step()
            step += 1
            if i % 128 == 0:
                suffix = 'Epoch [{}/{}] Loss {:.3f}'\
                    .format(epoch+1, epochs, loss.item())
                #print 'Epoch [{}/{}], Batch [{},{}], Loss {}'\
                #    .format(epoch, epochs, i, len(train_loader), loss.item())
                progress(i, len(train_loader), suffix)
                with open(LOSS_SAVE_PATH, 'a') as lossfile:
                    lossfile.write('{},{}\n'.format(step, loss.item()))
            del model_adv
            del adversary
        model.eval()
        torch.save(model.state_dict(), MODEL_SAVE_PATH.format(name, epoch))
        model.train()
        """
        model.eval()
        correct = 0
        for i, (images, labels) in enumerate(val_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
        model.train()
        # Length of val_loader is the number of BATCHES in the loader 
        # not the number of elements
        print 'Validation Accuracy = ' + str(float(correct)/N_VAL)
        with open(ACC_SAVE_PATH, 'a') as accfile:
            accfile.write('{},{}\n'.format(epoch, float(correct)/N_VAL))
        torch.save(model.state_dict(), MODEL_SAVE_PATH.format(name, epoch))
        """
        if (epoch+1) % int(epochs/4) == 0:
            print '\nDecreasing the learning rate to ' + str(lr/5)
            update_learning_rate(optimizer, lr, lr/5)
            lr = lr/5
            print 64*'-'

    print('\nTraining complete')
    model.eval()
    max_acc = {'acc': 0.0, 'epoch': -1}
    for epoch in range(epochs):
        model.load_state_dict(torch.load(MODEL_SAVE_PATH.format(name, epoch)))
        #model.eval()
        correct = 0
        for _, (images, labels) in enumerate(val_loader):
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
        acc = float(correct)/N_VAL
        print 'Epoch {} Validation Accuracy {}'.format(epoch, acc)
        if acc > max_acc['acc']:
            max_acc['acc'] = acc
            max_acc['epoch'] = epoch
        with open(ACC_SAVE_PATH, 'a') as accwriter:
            accwriter.write('{},{}\n'.format(epoch, acc))
    print('Validation complete')
    print('Maximum accuracy of {} observed at epoch {}'.format(max_acc['acc'], max_acc['epoch']))
    print('Keeping only epoch {}'.format(max_acc['epoch']))
    for epoch in range(epochs):
        if(epoch == max_acc['epoch']):
            shutil.move(MODEL_SAVE_PATH.format(name, epoch), EVAL_MODEL_PATH)
        else:
            os.remove(MODEL_SAVE_PATH.format(name, epoch))


def ifgsm_adv(**kwargs):
    raise NotImplementedError('I have not implemented this function yet')

