import torch
from torch.autograd import Variable
from torch.autograd.gradcheck import zero_gradients
class IterativeGradientAttack():
    def __init__(self, model, loss, eps=0.3, steps=40,\
            clip_min=0., clip_max=1.):
        assert model is not None
        assert loss is not None
        assert model.training is not True
        self.model = model
        self.loss = loss
        self.eps = eps
        self.steps = steps
        self.clip_min = clip_min
        self.clip_max = clip_max
    def __call__(self, image, target):
        return self.perturb(image, target)
    def perturb(self, image, target):
        assert image is not None 
        assert target is not None
        tar = Variable(target.to('cuda'))
        img = image.to('cuda')
        eps_step = self.eps/self.steps
        for step in range(self.steps):
            img = Variable(img, requires_grad=True)
            zero_gradients(img)
            self.model.zero_grad()
            out = self.model(img)
            cost = self.loss(out, tar)
            cost.backward()
            perturbation = eps_step * torch.sign(img.grad.data)
            adv = img.data + perturbation.to('cuda').float()
            img = torch.clamp(adv, self.clip_min, self.clip_max)
        return img

