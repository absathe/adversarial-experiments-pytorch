import json
import glob
import sys
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
import torch.utils.data as data_utils

def get_mnist_data_loaders(batch_size=64, n_train=50000, n_val=10000, n_test=10000):
    
    dataset_transform = transforms.Compose([transforms.ToTensor()])
    train_set = torchvision.datasets.MNIST(root='./mnist_data', download=True, \
                    train=True, transform=dataset_transform)
    test_set = torchvision.datasets.MNIST(root='./mnist_data', download=True, \
                    train=False, transform=dataset_transform)

    # Generated as follows 
    # indices = np.arange(0, 60000)
    # np.random.shuffle(indices)
    # np.save(...)
    indices = np.load('./MNIST_indices.npy')
    train_sampler = SubsetRandomSampler(indices[:n_train])
    val_sampler = SubsetRandomSampler(indices[n_train:])
    test_sampler = SubsetRandomSampler(np.arange(n_test))

    train_loader = data_utils.DataLoader(train_set, batch_size=batch_size, \
                        sampler=train_sampler)
    val_loader = data_utils.DataLoader(train_set, batch_size=batch_size, \
                        sampler=val_sampler)
    test_loader = data_utils.DataLoader(test_set, batch_size=batch_size, \
                        sampler=test_sampler)

    return train_loader, val_loader, test_loader

def progress(curr, total, suffix=''):
    bar_len = 48
    filled = int(round(bar_len * curr / float(total)))
    bar = '='*(filled-1) + '>' + '-'*(bar_len - filled)
    sys.stdout.write('\r[%s] .. %s' % (bar, suffix))
    sys.stdout.flush()

def get_config_from_file(config):
    with open(config, 'r') as f:
        data = json.load(f)
    return data

def truncated_normal(mean=0.0, std=1.0, l=1):
    """
    Generates values from normal distribution with `mean` and `std`,
    values whose magnitude is more than `2*std` are dropped and re-picked
    Returns vector of length l
    """
    samples = []
    for i in range(l):
        while True:
            sample = np.random.normal(mean, std)
            if np.abs(sample) <= 2 * std:
                # Valid sample, break out of while loop
                break
        samples.append(sample)
    assert len(samples) == l, "Something went wrong, length of samples({}) is not matching with the length parameter passed({})"\
    .format(len(samples), l)
    if l == 1:
        return samples[0]
    else:
        return np.array(samples)
def get_eval_params():
    """
    Opens `configs/eval.json`, puts into dictionary and returns
    """
    with open('configs/eval.json', 'r') as f:
        data = json.load(f)
    return data

def get_ifgsm_eps_params():
    """
    Opens `configs/eval_ifgsm.json`, puts into dictionary and returns
    """
    with open('configs/eval_ifgsm.json', 'r') as f:
        data = json.load(f)
    return data

def get_fgsm_eps_params():
    """
    Opens `configs/eval_fgsm.json`, puts into dictionary and returns
    """
    with open('configs/eval_fgsm.json','r') as f:
        data = json.load(f)
    return data

def get_pgd_eps_params():
    """
    Opens `configs/eval_pgd.json`, puts into dictionary and returns
    """
    with open('configs/eval_pgd.json', 'r') as f:
        data = json.load(f)
    return data

def get_pgd_config():
    """
    Opens `configs/pgd_config.json`, puts into dictionary and returns
    """
    with open('configs/pgd_config.json', 'r') as f:
        data = json.load(f)
    return data
    
def get_ifgsm_config():
    """
    Opens `configs/ifgsm_config.json`, puts into dictionary and returns
    """
    with open('configs/ifgsm_config.json', 'r') as f:
        data = json.load(f)
    return data

def get_fgsm_config():
    """
    Opens `configs/fgsm_config.json`, puts into dictionary and returns
    """
    with open('configs/fgsm_config.json', 'r') as f:
        data = json.load(f)
    return data
