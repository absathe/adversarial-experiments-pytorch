# Configuration Files

```
eval_* files denote the files used at the time of evaluation

For example : 
if pgd flag is set in eval.json, eval_pgd.json describes the epsilon steps
for evaluation. For generating these adversarial samples, pgd_config* files
will be used to get hyperparameters for PGD attack
```
