import numpy as np
import torch
import torch.nn as nn
import torchvision
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
from torchvision.datasets import MNIST
from autoencoder import AutoEncoder
import os
import sys

save_path = './ae-outputs/'
batch_dist = 'dist_batch.csv'
loss_file = 'loss_file.csv'
batch_size = 64
lr = 1e-4
n_epochs = 150

def to_img(x):
    y = (x + 1) / 2
    return y

def progress(current, total, suffix=''):
    bar_len = 48
    filled = int(round(bar_len * current / float(total)))
    if current >= total:
        bar = '=' * bar_len
        sys.stdout.write('\r[%s] .. %s Completed\n' % (bar, suffix))
        sys.stdout.flush()
        return
    bar = '=' * (filled - 1) + '>' + '-'*(bar_len - filled)
    sys.stdout.write('\r[%s] .. %s' % (bar, suffix))
    sys.stdout.flush()

def get_noisy_batch(clean_x, eps=0.3):
    rnd = torch.Tensor(np.random.normal(0, 1, tuple(clean_x.size()))).to('cuda')
    rnd = torch.sign(rnd)
    noisy_x = clean_x + eps * rnd
    noisy_x = torch.clamp(noisy_x,0,1)
    return noisy_x

def L2_Norm(grad):
    grad = grad.view(grad.size(0),-1)
    grad = torch.pow(grad,2)
    grad = grad.sum(1)
    grad = grad.pow(0.5)
    #grad = grad.mean(0)
    return grad

def dist_metrics(scores_clean,label_clean):
        #Inter class distance computation
        B        = scores_clean.size(0)
        idx      = torch.randperm(B).long().to('cuda')
        shuffled_scores_clean = torch.index_select(scores_clean, 0, idx)
        shuffled_label_clean  = torch.index_select(label_clean, 0, idx)
        diff_class_flag = (shuffled_label_clean - label_clean).float().abs()
        diff_class_flag = torch.clamp(diff_class_flag,0,1)
        same_class_flag = 1 - diff_class_flag
        
        diff_class_count= diff_class_flag.sum(0) + 1e-6 
        same_class_count= same_class_flag.sum(0) + 1e-6
        
        scores_dist = L2_Norm(scores_clean-shuffled_scores_clean)
        intra_class_dist = scores_dist*same_class_flag.detach()
        intra_class_dist = intra_class_dist.sum(0)/same_class_count.detach()
        
        inter_class_dist = scores_dist*diff_class_flag.detach()
        
        inter_class_dist = inter_class_dist.sum(0)/diff_class_count.detach()
        return intra_class_dist,inter_class_dist


def main():
    if not os.path.exists(save_path):
        os.mkdir(save_path)

    img_transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,),(0.5,))])
    dataset = MNIST('./data', download=True, transform=img_transform)

    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    """
    enc = Encoder().to('cuda')
    dec = Decoder().to('cuda')
    """
    model = AutoEncoder().to('cuda')
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    f = open(save_path + '/' + batch_dist, 'w')
    f.truncate(0)
    f.close()
    f = open(save_path + '/' + loss_file, 'w')
    f.truncate(0)
    f.close()
    step = 0
    for epoch in range(n_epochs):
        for data in dataloader:
            img,label = data
            img = img.to('cuda')
            label = label.to('cuda')
            B,C,H,W = img.size()
            #index = torch.randperm(B).to('cuda')
            #torch.index_select(img, 0, index)
            noisy_img = get_noisy_batch(img)
            
            # Forward pass 
            optimizer.zero_grad()
            output = model(img)
            loss = criterion(output,img)
            #loss = torch.abs(output - img).view(B, -1).sum(1).sum(0) / (B*C*H*W)
            # Backward pass
            loss.backward()
            optimizer.step()
            
            # Compute distances
            model.eval()
            enc_clean = model.encoder_op(img)
            enc_noisy = model.encoder_op(noisy_img)
            per_dist = L2_Norm(enc_clean - enc_noisy).mean(0)
            intra_class_dist, inter_class_dist = dist_metrics(enc_clean,label)
            if step % 256 == 0:
                with open(save_path + '/' + batch_dist, 'a') as f:
                    f.write('{},{},{},{}\n'.format(step, per_dist, intra_class_dist, inter_class_dist))
                with open(save_path + '/' + loss_file, 'a') as f:
                    f.write('{}, {}\n'.format(step, loss.item()))
            model.train()
            step += 1
        if epoch % 10 == 0:
            pic = to_img(output.cpu().data)
            save_image(pic, save_path + '/img_at_epoch_{}.png'.format(epoch))
        progress(epoch+1, n_epochs, suffix='Eposh [{}/{}] Loss {:.5f}'.format(epoch+1, n_epochs, loss.item()))

    torch.save(model.state_dict(), './saved_weights_ae.pt')

if __name__ == '__main__':
    main()
