#!/bin/bash

GPU=2
EPOCHS=20
LEARNING_RATE=1e-2
BATCH_SIZE=64
SAVEDIR='./saved_models/'
MODE='train'
MODEL='B'
TRAIN='normal'

CUDA_VISIBLE_DEVICES=$GPU python main.py --mode $MODE --train $TRAIN \
 --model $MODEL	--epochs $EPOCHS --lr $LEARNING_RATE --batch_size $BATCH_SIZE \
 --savedir $SAVEDIR
