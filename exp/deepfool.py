'''
Deepfool attack
'''
def L2Norm_sample(per):
    B,C,H,W = per.size()
    per = per.view(B,-1)
    per = per * per
    per = per.sum(1)
    per = torch.pow(per,0.5)
    return per

def model_get_grad_predval(model,x):
    x_in    = Variable(torch.FloatTensor(x).cuda(),requires_grad=True)
    zero_gradients(x_in)
    out     = model(x_in)
    # sort score in decending order for each samples
    out_s,idx = torch.sort(out, dim=1, descending=True)
    # get gradients
    B,C,H,W = x_in.size()
    Classes = out.size()[1]
    gradients = np.zeros([B,Classes,C,H,W])
    for i in range(out.size(1)):
        zero_gradients(x_in)
        out_s[:,i].backward(torch.ones(B).cuda(),retain_variables=True)
        grad =  x_in.grad
        grad =  grad.view(B,1,C,H,W)
        grad =  grad.data.cpu().numpy()
        gradients[:,i:i+1,:,:,:] = grad
    predval = out_s.data.cpu().numpy()
    return predval,gradients

def model_argmax(model,x):
    zero_gradients(x)
    x_in    = Variable(torch.FloatTensor(x).cuda())
    out     = model(x_in)
    idx     = out.max(1)[1]
    idx     = idx.data.cpu().numpy()
    return idx


def deepfool_attack(model,sample,nb_candidate=10,overshoot=0.02, max_iter=10, clip_min=0, clip_max=1,max_l2=1):
    sample = sample.numpy()
    adv_x = copy.copy(sample)
    # Initialize the loop variables
    iteration = 0
    current = model_argmax(model,adv_x)
    if current.shape == ():
        current = np.array([current])
    w = np.squeeze(np.zeros(sample.shape[1:]))  # same shape as original image
    r_tot = np.zeros(sample.shape)
    original = current  # use original label as the reference
    # Repeat this main loop until we have achieved misclassification
    while (np.any(current == original) and iteration < max_iter):
        predictions_val , gradients = model_get_grad_predval(model,adv_x)
        for idx in range(sample.shape[0]):
            pert = np.inf
            if current[idx] != original[idx]:
                continue
            for k in range(1, nb_candidate):
                w_k = gradients[idx, k, ...] - gradients[idx, 0, ...]
                f_k = predictions_val[idx, k] - predictions_val[idx, 0]
                # adding value 0.00001 to prevent f_k = 0
                pert_k = (abs(f_k) + 0.00001) / np.linalg.norm(w_k.flatten())
                if pert_k < pert:
                    pert = pert_k
                    w = w_k
            r_i = pert * w / np.linalg.norm(w)
            r_tot[idx, ...] = r_tot[idx, ...] + r_i
        #modfied section
        r_tot =  torch.FloatTensor(r_tot).cuda()
        adv_x = r_tot + torch.FloatTensor(sample).cuda()
        adv_x = torch.clamp(adv_x, clip_min, clip_max)
        adv_x =  adv_x.cpu().numpy()
        r_tot =  r_tot.cpu().numpy()
        #adv_x   = np.clip(mask*r_tot + sample, clip_min,  clip_min)
        current =  model_argmax(model,adv_x)
        if current.shape == ():
            current = np.array([current])
        # Update loop variables
        iteration = iteration + 1
    adv_x = np.clip((1+overshoot)*r_tot + sample, clip_min, clip_max)
    adv_x = torch.FloatTensor(adv_x)
    return adv_x