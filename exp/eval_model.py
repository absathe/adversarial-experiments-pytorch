from pprint import pprint
import time
import datetime
import numpy as np
import torch
import torch.nn as nn
import utils
import torchvision.transforms as transforms
from utils import get_mnist_data_loaders
from autoencoder import AutoEncoder
from attacks import IterativeGradientAttack, ProjectedGradientDescent, DeepFool
from advertorch.attacks import LinfBasicIterativeAttack, GradientSignAttack, LinfPGDAttack, CarliniWagnerL2Attack
EVAL_SAVE_PATH = 'model_{}'
EVAL_MODEL_NAME = 'model{}.pt'
EVAL_ID = 'id.txt'
EVAL_PGD_NAME = 'eps_vs_pgd_model_{}.txt'
EVAL_FGSM_NAME = 'eps_vs_fgsm_model_{}.txt'
EVAL_IFGSM_NAME = 'eps_vs_ifgsm_model_{}.txt'
EVAL_CW_NAME = 'c&wl2.txt'
EVAL_DEEPFOOL_NAME = 'deepfool.txt'
EVAL_TIME = 'eval_times.txt'
N_TRAIN = 50000
N_VAL = 10000
N_TEST = 10000


def to_autoencoder(images):
    """
    `images` is a tensor from [0,1]
    Function will return it converted to [-1,1]
    -------------------------------------------
    Before passing images to autoencoder, use 
    this function
    """
    images = images - 0.5 # [makes it [-0.5,0.5]]
    images = images * 2
    return images

def from_autoencoder(images):
    """
    `images` is a tensor returned from autoencoder
    Function will return it converted to [0,1]
    ----------------------------------------------
    After getting images from autoencoder, use 
    this function
    """
    images = images + 1
    images = images * 2
    return images


def get_evaluate_data(model, data_loader, loss_fn=None, adversary=None, autoenc=None):
    """
    loss_fn should have `size_average=False`
    """
    correct = 0
    loss_sum = 0
    assert data_loader is not None
    for i, (images, labels) in enumerate(data_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        if autoenc is not None:
            images = to_autoencoder(images)
            images = autoenc(images)
            images = from_autoencoder(images)
        if adversary is not None:
            images = adversary(images, labels)
        model.zero_grad()
        outputs = model(images)
        if loss_fn is not None:
            loss_sum += loss_fn(outputs, labels).data
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
        utils.progress(i, len(data_loader), 'Batch {}/{}'.format(i, len(data_loader)))
    return correct, loss_sum

def evaluate(eval_model, name, savedir, eval_model_cat, batch_size, config):
    EVAL_DIR = (savedir + '/' + eval_model_cat + '/' + EVAL_SAVE_PATH).format(name)
    EVAL_MODEL_LOAD_PATH = (EVAL_DIR + '/' + EVAL_MODEL_NAME).format(name)
    model = eval_model.to('cuda')
    model.load_state_dict(torch.load(EVAL_MODEL_LOAD_PATH))
    model.eval()
    
    autoenc = AutoEncoder().to('cuda')
    autoenc.load_state_dict(torch.load(savedir + '/saved_weights_ae.pt'))

    small_add = 1e-2
    eval_loss_fn = nn.CrossEntropyLoss(size_average=False)
    adv_loss_fn = nn.CrossEntropyLoss()
    advertorch_loss_fn = nn.CrossEntropyLoss(size_average=False)

    _, val_loader, test_loader = get_mnist_data_loaders(batch_size)
    print 64 * '-'
    print 'Evaluating on clean samples:'
    print 64 * '-'
    correct_val, loss_val = get_evaluate_data(model, val_loader, loss_fn=eval_loss_fn, autoenc=autoenc)
    correct_test, loss_test = get_evaluate_data(model, test_loader, loss_fn=eval_loss_fn, autoenc=autoenc)
    print('Validation Set Accuracy : {}%'.format(float(correct_val) * 100/N_VAL))
    print('Average Test Set Loss : {}'.format(loss_val/N_VAL))
    print('Test Set Accuracy : {}%'.format(float(correct_test) * 100/N_VAL))
    print('Average Test Set Loss : {}'.format(loss_test/N_VAL))
    with open(EVAL_DIR + '/' + EVAL_ID, 'w') as f:
        f.write('Model name : {}\n'.format(name))
        f.write('Validation Set Accuracy : {}%\n'.format(float(correct_val) * 100/N_VAL))
        f.write('Average Validation Set Loss : {}\n'.format(loss_val/N_VAL))
        f.write('Training Mode : {}\n'.format(eval_model_cat))
        f.write('Test Set Accuracy : {}%\n'.format(float(correct_test) * 100/N_TEST))
        f.write('Average Test Set Loss : {}\n'.format(loss_test/N_TEST))
    ############## CLEAN TESTING DONE #############
    print 64 * '-'
    config = utils.get_config_from_file(config)
    config = config['eval']
    print 'Configuration loaded'
    #pprint(config)
    print 'Evaluating against adversarial attacks : '
    if config['cw'] == True:
        print '-' * 64
        print 'Carlini & Wagner L2 attack'
        print '-' * 64
        cw_configs = config['cw_params']
        f = open(EVAL_DIR + '/' + EVAL_CW_NAME, 'w')
        f.truncate(0)
        f.close()
        for cw_params in cw_configs:
            start_time = time.time()
            print(cw_params)
            correct_cw = 0
            total_l2_norm = 0
            num_classes, learning_rate, search_steps, max_iterations, initial_const, clip_min, clip_max = cw_params['num_classes'],\
                cw_params['learning_rate'], cw_params['search_steps'], cw_params['max_iterations'], cw_params['initial_const'], \
                cw_params['clip_min'], cw_params['clip_max']
            adversary = CarliniWagnerL2Attack(predict=model, num_classes=num_classes, learning_rate=learning_rate,\
                binary_search_steps=search_steps, max_iterations=max_iterations, initial_const=initial_const, clip_min=clip_min, clip_max=clip_max)
            for i, (images, labels) in enumerate(test_loader):
                images, labels = images.to('cuda'), labels.long().to('cuda')
                adv_images = adversary.perturb(images, labels)
                outputs = model(adv_images)
                _, predictions = torch.max(outputs.data, 1)
                labels = labels.to('cuda')
                correct_cw += (predictions == labels).sum().item()
                total_l2_norm += np.linalg.norm(adv_images.to('cpu').numpy() - images.to('cpu').numpy())
                utils.progress(i, len(test_loader), 'Batch {}/{}'.format(i, len(test_loader)))
             
            print('\nCorrect = {} Total L2 Norm = {}'.format(correct_cw, total_l2_norm))
            print('Accuracy = {} Average L2 norm = {}'.format(correct_cw * 100.0/N_TEST, total_l2_norm / N_TEST))
            print(32 * '-')
            with open(EVAL_DIR + '/' + EVAL_CW_NAME, 'a') as f:
                f.write(str(cw_params) + '\n')
                f.write('Accuracy = {}\n'.format(correct_cw * 100.0/N_TEST))
                f.write('Average L2 norm = {}\n'.format(total_l2_norm / N_TEST))
                f.write(64*'-' + '\n')
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on C&W L2 attack' + '\n')
                f.write(str(cw_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
    if config['deepfool'] == True:
        print '-' * 64
        print 'DeepFool L2 attack'
        print '-' * 64
        df_configs = config['deepfool_params']
        f = open(EVAL_DIR + '/' + EVAL_DEEPFOOL_NAME, 'w')
        f.truncate(0)
        f.close()
        for df_params in df_configs:
            print(df_params)
            start_time = time.time()
            correct_deepfool = 0
            total_l2_norm = 0
            max_iter, clip_min, clip_max, max_l2, nb_candidate, overshoot = df_params['max_iter'], df_params['clip_min'], \
                df_params['clip_max'], df_params['max_l2'], df_params['nb_candidate'], df_params['overshoot']
            adversary = DeepFool(model, max_iter=max_iter, clip_min=clip_min, clip_max=clip_max, nb_candidate=nb_candidate, overshoot=overshoot)
            for i, (images, labels) in enumerate(test_loader):
                adv_images = adversary(images).to('cuda')
                outputs = model(adv_images)
                _, predictions = torch.max(outputs.data, 1)
                labels = labels.to('cuda')
                correct_deepfool += (predictions == labels).sum().item()
                total_l2_norm += np.linalg.norm(adv_images.to('cpu').numpy() - images.to('cpu').numpy())
                utils.progress(i, len(test_loader), 'Batch {}/{}'.format(i, len(test_loader)))
                
            print('\nCorrect = {} Total L2 Norm = {}'.format(correct_deepfool, total_l2_norm))
            print('Accuracy = {} Average L2 norm = {}'.format(correct_deepfool * 100.0/N_TEST, total_l2_norm / N_TEST))
            with open(EVAL_DIR + '/' + EVAL_DEEPFOOL_NAME, 'a') as f:
                f.write(str(df_params) + '\n')
                f.write('Accuracy = {}\n'.format(correct_deepfool * 100.0/N_TEST))
                f.write('Average L2 norm = {}\n'.format(total_l2_norm / N_TEST))
                f.write(64*'-' + '\n')
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on DeepFool L2 attack' + '\n')
                f.write(str(df_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
    if config['fgsm'] == True:
        ##########  Fast Gradient Sign Method #########
        print 64 * '-'
        print 64 * '-'
    
        fgsm_configs = config['fgsm_params']
        for fgsm_params in fgsm_configs:
            eps_min, eps_max, eps_step  = fgsm_params['eps_min'], fgsm_params['eps_max'], fgsm_params['eps_step']
            clip_min, clip_max = fgsm_params['clip_min'], fgsm_params['clip_max']
            SAVE_NAME = name + '[' + str(eps_min) + ',' + str(eps_max) + ',' + str(eps_step) + ']' 

            print 'Fast Gradient Sign Method'
            print fgsm_params
            start_time = time.time()
            f = open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME), 'w')
            f.truncate(0)
            f.close()
            f = open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME + '_advertorch'), 'w')
            f.truncate(0)
            f.close()
            for eps in np.arange(eps_min, eps_max + small_add, eps_step):
                adversary = IterativeGradientAttack(model, adv_loss_fn, eps=eps, clip_min=0., \
                    clip_max=1., steps=1)
                advertorch = GradientSignAttack(model, advertorch_loss_fn, eps=eps.item(), clip_min=0., \
                    clip_max=1., targeted=False)
                correct_normal, loss_normal = get_evaluate_data(model, test_loader, eval_loss_fn, adversary, autoenc)
                correct_advertorch, loss_advertorch = get_evaluate_data(model, test_loader, eval_loss_fn, advertorch, autoenc)
                print('Using own implementation: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_normal) * 100 / N_TEST, float(loss_normal) / N_TEST))
                print('Using advertorch: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_advertorch) * 100 / N_TEST, float(loss_advertorch) / N_TEST))
                with open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_normal)/N_TEST, float(loss_normal)/N_TEST))
                with open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME + '_advertorch'), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_advertorch)/N_TEST, float(loss_advertorch)/N_TEST))
                print 32 * '-'
                del adversary
                del advertorch
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on FGSM attack\n')
                f.write(str(fgsm_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
    if config['ifgsm'] == True:
        ##### Iterative Fast Gradient Sign Method #####
        print 64 * '-'
        print 64 * '-'

        ifgsm_configs = config['ifgsm_params']
        for ifgsm_params in ifgsm_configs:
            eps_min, eps_max, eps_step = ifgsm_params['eps_min'], ifgsm_params['eps_max'], ifgsm_params['eps_step']
            eps_iter, steps, clip_min, clip_max = ifgsm_params['eps_iter'], ifgsm_params['nb_iter'], ifgsm_params['clip_min'],  ifgsm_params['clip_max']

            SAVE_NAME = name + '[' + str(eps_min) + ',' + str(eps_max) + ',' + str(eps_step) + ']' + 'iter' + str(steps)
            
            print 'Iterative Fast Gradient Sign Method : '
            print ifgsm_params
            start_time = time.time()
            f = open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME), 'w')
            f.truncate(0)
            f.close()
            f = open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME + '_advertorch'), 'w')
            f.truncate(0)
            f.close()
            for eps in np.arange(eps_min, eps_max + small_add, eps_step):
                adversary = IterativeGradientAttack(model, adv_loss_fn, eps=eps, clip_min=clip_min, \
                    clip_max=clip_max, steps=steps)
                if eps_iter == -1:
                    eps_adv_iter = eps.item() / steps
                else:
                    eps_adv_iter = eps_iter
                advertorch = LinfBasicIterativeAttack(model, advertorch_loss_fn, eps=eps.item(), clip_min=clip_min, \
                    clip_max=clip_max, targeted=False, eps_iter=eps_adv_iter, nb_iter=steps)
                correct_normal, loss_normal = get_evaluate_data(model, test_loader, eval_loss_fn, adversary, autoenc)
                correct_advertorch, loss_advertorch = get_evaluate_data(model, test_loader, eval_loss_fn, advertorch, autoenc)
                print('Using own implementation: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_normal) * 100 / N_TEST, float(loss_normal) / N_TEST))
                print('Using advertorch: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_advertorch) * 100 / N_TEST, float(loss_advertorch) / N_TEST))
                with open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_normal)/N_TEST, float(loss_normal)/N_TEST))
                with open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME + '_advertorch'), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_advertorch)/N_TEST, float(loss_advertorch)/N_TEST))
                print 32 * '-'
                del adversary
                del advertorch 
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on I-FGSM attack\n')
                f.write(str(ifgsm_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')  
    if config['pgd'] == True:
        print 64 * '-'
        print 64 * '-'
        pgd_configs = config['pgd_params']
        for pgd_params in pgd_configs:
            eps_min, eps_max, eps_step = pgd_params['eps_min'], pgd_params['eps_max'], pgd_params['eps_step']
            eps_iter, steps, clip_min, clip_max = pgd_params['eps_iter'], pgd_params['nb_iter'], pgd_params['clip_min'], pgd_params['clip_max']
            print 'Projected Gradient Descent'
            print pgd_params
            start_time = time.time()
            SAVE_NAME = name + '[' + str(eps_min) + ',' + str(eps_max) + ',' + str(eps_step) + ']' + 'iter' + str(steps)
            f = open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME), 'w')
            f.truncate(0)
            f.close()
            f = open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME + '_advertorch'), 'w')
            f.truncate(0)
            f.close()
            for eps in np.arange(0, 0.3 + small_add, 0.03):
                adversary = ProjectedGradientDescent(model, adv_loss_fn, eps=eps, clip_min=clip_min, \
                    clip_max=clip_max, steps=steps, eps_iter=eps_iter)
                advertorch = LinfPGDAttack(model, advertorch_loss_fn, eps=eps.item(), clip_min=clip_min, \
                    clip_max=clip_max, targeted=False, eps_iter=eps_iter, nb_iter=steps)
                correct_normal, loss_normal = get_evaluate_data(model, test_loader, eval_loss_fn, adversary, autoenc)
                correct_advertorch, loss_advertorch = get_evaluate_data(model, test_loader, eval_loss_fn, advertorch, autoenc)
                print('Using own implementation: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_normal) * 100 / N_TEST, float(loss_normal) / N_TEST))
                print('Using advertorch: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_advertorch) * 100 / N_TEST, float(loss_advertorch) / N_TEST))
                with open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_normal)/N_TEST, float(loss_normal)/N_TEST))
                with open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME + '_advertorch'), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_advertorch)/N_TEST, float(loss_advertorch)/N_TEST))
                print 32 * '-'
                del adversary
                del advertorch
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on PGD attack\n')
                f.write(str(pgd_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')

def evaluate_grey(eval_model, name, savedir, eval_model_cat, batch_size, config):
    EVAL_DIR = (savedir + '/' + eval_model_cat + '/' + EVAL_SAVE_PATH).format(name)
    EVAL_MODEL_LOAD_PATH = (EVAL_DIR + '/' + EVAL_MODEL_NAME).format(name)
    model = eval_model.to('cuda')
    model.load_state_dict(torch.load(EVAL_MODEL_LOAD_PATH))
    model.eval()
    
    autoenc = AutoEncoder().to('cuda')
    autoenc.load_state_dict(torch.load(savedir + '/saved_weights_ae.pt'))

    small_add = 1e-2
    eval_loss_fn = nn.CrossEntropyLoss(size_average=False)
    adv_loss_fn = nn.CrossEntropyLoss()
    advertorch_loss_fn = nn.CrossEntropyLoss(size_average=False)

    _, val_loader, test_loader = get_mnist_data_loaders(batch_size)
    print 64 * '-'
    print 'Evaluating on clean samples:'
    print 64 * '-'
    correct_val, loss_val = get_evaluate_data(model, val_loader, loss_fn=eval_loss_fn, autoenc=autoenc)
    correct_test, loss_test = get_evaluate_data(model, test_loader, loss_fn=eval_loss_fn, autoenc=autoenc)
    print('Validation Set Accuracy : {}%'.format(float(correct_val) * 100/N_VAL))
    print('Average Test Set Loss : {}'.format(loss_val/N_VAL))
    print('Test Set Accuracy : {}%'.format(float(correct_test) * 100/N_VAL))
    print('Average Test Set Loss : {}'.format(loss_test/N_VAL))
    with open(EVAL_DIR + '/' + EVAL_ID, 'w') as f:
        f.write('Model name : {}\n'.format(name))
        f.write('Validation Set Accuracy : {}%\n'.format(float(correct_val) * 100/N_VAL))
        f.write('Average Validation Set Loss : {}\n'.format(loss_val/N_VAL))
        f.write('Training Mode : {}\n'.format(eval_model_cat))
        f.write('Test Set Accuracy : {}%\n'.format(float(correct_test) * 100/N_TEST))
        f.write('Average Test Set Loss : {}\n'.format(loss_test/N_TEST))
    ############## CLEAN TESTING DONE #############
    print 64 * '-'
    config = utils.get_config_from_file(config)
    config = config['eval']
    print 'Configuration loaded'
    #pprint(config)
    print 'Evaluating against adversarial attacks : '
    if config['cw'] == True:
        print '-' * 64
        print 'Carlini & Wagner L2 attack'
        print '-' * 64
        cw_configs = config['cw_params']
        f = open(EVAL_DIR + '/' + EVAL_CW_NAME, 'w')
        f.truncate(0)
        f.close()
        for cw_params in cw_configs:
            start_time = time.time()
            print(cw_params)
            correct_cw = 0
            total_l2_norm = 0
            num_classes, learning_rate, search_steps, max_iterations, initial_const, clip_min, clip_max = cw_params['num_classes'],\
                cw_params['learning_rate'], cw_params['search_steps'], cw_params['max_iterations'], cw_params['initial_const'], \
                cw_params['clip_min'], cw_params['clip_max']
            adversary = CarliniWagnerL2Attack(predict=model, num_classes=num_classes, learning_rate=learning_rate,\
                binary_search_steps=search_steps, max_iterations=max_iterations, initial_const=initial_const, clip_min=clip_min, clip_max=clip_max)
            for i, (images, labels) in enumerate(test_loader):
                images, labels = images.to('cuda'), labels.long().to('cuda')
                adv_images = adversary.perturb(images, labels)
                outputs = model(adv_images)
                _, predictions = torch.max(outputs.data, 1)
                labels = labels.to('cuda')
                correct_cw += (predictions == labels).sum().item()
                total_l2_norm += np.linalg.norm(adv_images.to('cpu').numpy() - images.to('cpu').numpy())
                utils.progress(i, len(test_loader), 'Batch {}/{}'.format(i, len(test_loader)))
             
            print('\nCorrect = {} Total L2 Norm = {}'.format(correct_cw, total_l2_norm))
            print('Accuracy = {} Average L2 norm = {}'.format(correct_cw * 100.0/N_TEST, total_l2_norm / N_TEST))
            print(32 * '-')
            with open(EVAL_DIR + '/' + EVAL_CW_NAME, 'a') as f:
                f.write(str(cw_params) + '\n')
                f.write('Accuracy = {}\n'.format(correct_cw * 100.0/N_TEST))
                f.write('Average L2 norm = {}\n'.format(total_l2_norm / N_TEST))
                f.write(64*'-' + '\n')
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on C&W L2 attack' + '\n')
                f.write(str(cw_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
    if config['deepfool'] == True:
        print '-' * 64
        print 'DeepFool L2 attack'
        print '-' * 64
        df_configs = config['deepfool_params']
        f = open(EVAL_DIR + '/' + EVAL_DEEPFOOL_NAME, 'w')
        f.truncate(0)
        f.close()
        for df_params in df_configs:
            print(df_params)
            start_time = time.time()
            correct_deepfool = 0
            total_l2_norm = 0
            max_iter, clip_min, clip_max, max_l2, nb_candidate, overshoot = df_params['max_iter'], df_params['clip_min'], \
                df_params['clip_max'], df_params['max_l2'], df_params['nb_candidate'], df_params['overshoot']
            adversary = DeepFool(model, max_iter=max_iter, clip_min=clip_min, clip_max=clip_max, nb_candidate=nb_candidate, overshoot=overshoot)
            for i, (images, labels) in enumerate(test_loader):
                adv_images = adversary(images).to('cuda')
                outputs = model(adv_images)
                _, predictions = torch.max(outputs.data, 1)
                labels = labels.to('cuda')
                correct_deepfool += (predictions == labels).sum().item()
                total_l2_norm += np.linalg.norm(adv_images.to('cpu').numpy() - images.to('cpu').numpy())
                utils.progress(i, len(test_loader), 'Batch {}/{}'.format(i, len(test_loader)))
                
            print('\nCorrect = {} Total L2 Norm = {}'.format(correct_deepfool, total_l2_norm))
            print('Accuracy = {} Average L2 norm = {}'.format(correct_deepfool * 100.0/N_TEST, total_l2_norm / N_TEST))
            with open(EVAL_DIR + '/' + EVAL_DEEPFOOL_NAME, 'a') as f:
                f.write(str(df_params) + '\n')
                f.write('Accuracy = {}\n'.format(correct_deepfool * 100.0/N_TEST))
                f.write('Average L2 norm = {}\n'.format(total_l2_norm / N_TEST))
                f.write(64*'-' + '\n')
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on DeepFool L2 attack' + '\n')
                f.write(str(df_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
    if config['fgsm'] == True:
        ##########  Fast Gradient Sign Method #########
        print 64 * '-'
        print 64 * '-'
    
        fgsm_configs = config['fgsm_params']
        for fgsm_params in fgsm_configs:
            eps_min, eps_max, eps_step  = fgsm_params['eps_min'], fgsm_params['eps_max'], fgsm_params['eps_step']
            clip_min, clip_max = fgsm_params['clip_min'], fgsm_params['clip_max']
            SAVE_NAME = name + '[' + str(eps_min) + ',' + str(eps_max) + ',' + str(eps_step) + ']' 

            print 'Fast Gradient Sign Method'
            print fgsm_params
            start_time = time.time()
            f = open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME), 'w')
            f.truncate(0)
            f.close()
            f = open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME + '_advertorch'), 'w')
            f.truncate(0)
            f.close()

            correct = 0
            adversary = IterativeGradientAttack(model, adv_loss_fn, eps=eps_max, clip_min=0., clip_max=1., steps=1)
            for _, (images, labels) in enumerate(test_loader):
                images, labels = images.to('cuda'), labels.to('cuda')
                # Generate adversarial samples
                images = adversary(images, labels)
                # Pass through autoencoder
                images = to_autoencoder(images)
                images = autoenc(images)
                images = from_autoencoder(images)
                # Pass through model to get outputs
                outputs = model(images)
                # Calculate accuracy
                _, predictions = torch.max(outputs, 1)
                correct += (predictions == labels).sum().item()
            with open(EVAL_DIR + '/' + 'fgsm_with_ae_preprocessor{}.txt'.format(eps_max), 'w') as f:
                f.write(str(fgsm_params) + '\n')
                f.write('Evaluated only at eps_max\n')
                f.write('Accuracy = {}\n'.format(float(correct) * 100 / N_TEST))
                print('Accuracy = {}\n'.format(float(correct) * 100 / N_TEST))
                f.write(64 * '-' + '\n')
            """
            for eps in np.arange(eps_min, eps_max + small_add, eps_step):
                adversary = IterativeGradientAttack(model, adv_loss_fn, eps=eps, clip_min=0., \
                    clip_max=1., steps=1)
                advertorch = GradientSignAttack(model, advertorch_loss_fn, eps=eps.item(), clip_min=0., \
                    clip_max=1., targeted=False)
                correct_normal, loss_normal = get_evaluate_data(model, test_loader, eval_loss_fn, adversary, autoenc)
                correct_advertorch, loss_advertorch = get_evaluate_data(model, test_loader, eval_loss_fn, advertorch, autoenc)
                print('Using own implementation: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_normal) * 100 / N_TEST, float(loss_normal) / N_TEST))
                print('Using advertorch: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_advertorch) * 100 / N_TEST, float(loss_advertorch) / N_TEST))
                with open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_normal)/N_TEST, float(loss_normal)/N_TEST))
                with open(EVAL_DIR + '/' + EVAL_FGSM_NAME.format(SAVE_NAME + '_advertorch'), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_advertorch)/N_TEST, float(loss_advertorch)/N_TEST))
                print 32 * '-'
                del adversary
                del advertorch
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on FGSM attack\n')
                f.write(str(fgsm_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
            """
    if config['ifgsm'] == True:
        ##### Iterative Fast Gradient Sign Method #####
        print 64 * '-'
        print 64 * '-'

        ifgsm_configs = config['ifgsm_params']
        for ifgsm_params in ifgsm_configs:
            eps_min, eps_max, eps_step = ifgsm_params['eps_min'], ifgsm_params['eps_max'], ifgsm_params['eps_step']
            eps_iter, steps, clip_min, clip_max = ifgsm_params['eps_iter'], ifgsm_params['nb_iter'], ifgsm_params['clip_min'],  ifgsm_params['clip_max']

            SAVE_NAME = name + '[' + str(eps_min) + ',' + str(eps_max) + ',' + str(eps_step) + ']' + 'iter' + str(steps)
            
            print 'Iterative Fast Gradient Sign Method : '
            print ifgsm_params
            start_time = time.time()
            f = open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME), 'w')
            f.truncate(0)
            f.close()
            f = open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME + '_advertorch'), 'w')
            f.truncate(0)
            f.close()
            for eps in np.arange(eps_min, eps_max + small_add, eps_step):
                adversary = IterativeGradientAttack(model, adv_loss_fn, eps=eps, clip_min=clip_min, \
                    clip_max=clip_max, steps=steps)
                if eps_iter == -1:
                    eps_adv_iter = eps.item() / steps
                else:
                    eps_adv_iter = eps_iter
                advertorch = LinfBasicIterativeAttack(model, advertorch_loss_fn, eps=eps.item(), clip_min=clip_min, \
                    clip_max=clip_max, targeted=False, eps_iter=eps_adv_iter, nb_iter=steps)
                correct_normal, loss_normal = get_evaluate_data(model, test_loader, eval_loss_fn, adversary, autoenc)
                correct_advertorch, loss_advertorch = get_evaluate_data(model, test_loader, eval_loss_fn, advertorch, autoenc)
                print('Using own implementation: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_normal) * 100 / N_TEST, float(loss_normal) / N_TEST))
                print('Using advertorch: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_advertorch) * 100 / N_TEST, float(loss_advertorch) / N_TEST))
                with open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_normal)/N_TEST, float(loss_normal)/N_TEST))
                with open(EVAL_DIR + '/' + EVAL_IFGSM_NAME.format(SAVE_NAME + '_advertorch'), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_advertorch)/N_TEST, float(loss_advertorch)/N_TEST))
                print 32 * '-'
                del adversary
                del advertorch 
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on I-FGSM attack\n')
                f.write(str(ifgsm_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')  
    if config['pgd'] == True:
        print 64 * '-'
        print 64 * '-'
        pgd_configs = config['pgd_params']
        for pgd_params in pgd_configs:
            eps_min, eps_max, eps_step = pgd_params['eps_min'], pgd_params['eps_max'], pgd_params['eps_step']
            eps_iter, steps, clip_min, clip_max = pgd_params['eps_iter'], pgd_params['nb_iter'], pgd_params['clip_min'], pgd_params['clip_max']
            print 'Projected Gradient Descent'
            print pgd_params
            start_time = time.time()
            SAVE_NAME = name + '[' + str(eps_min) + ',' + str(eps_max) + ',' + str(eps_step) + ']' + 'iter' + str(steps)
            f = open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME), 'w')
            f.truncate(0)
            f.close()
            f = open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME + '_advertorch'), 'w')
            f.truncate(0)
            f.close()
            for eps in np.arange(0, 0.3 + small_add, 0.03):
                adversary = ProjectedGradientDescent(model, adv_loss_fn, eps=eps, clip_min=clip_min, \
                    clip_max=clip_max, steps=steps, eps_iter=eps_iter)
                advertorch = LinfPGDAttack(model, advertorch_loss_fn, eps=eps.item(), clip_min=clip_min, \
                    clip_max=clip_max, targeted=False, eps_iter=eps_iter, nb_iter=steps)
                correct_normal, loss_normal = get_evaluate_data(model, test_loader, eval_loss_fn, adversary, autoenc)
                correct_advertorch, loss_advertorch = get_evaluate_data(model, test_loader, eval_loss_fn, advertorch, autoenc)
                print('Using own implementation: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_normal) * 100 / N_TEST, float(loss_normal) / N_TEST))
                print('Using advertorch: eps = {}: Accuracy = {} Avg Loss {} on test set'.\
                        format(eps, float(correct_advertorch) * 100 / N_TEST, float(loss_advertorch) / N_TEST))
                with open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_normal)/N_TEST, float(loss_normal)/N_TEST))
                with open(EVAL_DIR + '/' + EVAL_PGD_NAME.format(SAVE_NAME + '_advertorch'), 'a') as f:
                    f.write('{},{},{}\n'.format(eps, float(correct_advertorch)/N_TEST, float(loss_advertorch)/N_TEST))
                print 32 * '-'
                del adversary
                del advertorch
            with open(EVAL_DIR + '/' + EVAL_TIME, 'a') as f:
                f.write(str(datetime.datetime.now()) + '\n')
                f.write('Evaluated on PGD attack\n')
                f.write(str(pgd_params) + '\n')
                f.write('Total time elapsed : (time.time() - start_time) : {}\n'.format(time.time()-start_time))
                f.write(64 * '-' + '\n')
"""
    evaluate_fgsm(model, loss=nn.CrossEntropyLoss(), eps=0.3, clip_min=0., clip_max=1., \
                test_loader=test_loader)

    evaluate_ifgsm(model, loss=nn.CrossEntropyLoss(), eps=0.3, nb_iter=40, eps_iter=0.01, \
            clip_min=0., clip_max=1., test_loader=test_loader)

    evaluate_pgd(model, loss=nn.CrossEntropyLoss(), eps=0.3, nb_iter=40, eps_iter=0.01, \
            clip_min=0., clip_max=1., test_loader=test_loader)
    advertorch_evaluate_fgsm(model, loss=nn.CrossEntropyLoss(size_average=False), eps=0.3, \
        clip_min=0., clip_max=1., test_loader=test_loader)
    advertorch_evaluate_fgsm(model, loss=nn.CrossEntropyLoss(size_average=False), eps=0.3, \
        clip_min=0., clip_max=1., nb_iter=40, eps_iter=eps/nb_iter)
    advertorch_evaluate_pgd(model, loss=nn.CrossEntropyLoss(size_average=False), eps=0.1, \
        clip_min=0., clip_max=1., nb_iter=40, eps_iter=0.01)

def evaluate_fgsm(model, loss, eps, clip_min, clip_max, test_loader):
    attack = IterativeGradientAttack(model, loss, eps=eps, clip_min=clip_min, clip_max=clip_max, steps=1)
    correct = 0
    print 'Evaluating on FGSM attack: eps = {}'.format(eps)
    for i, (images, labels) in enumerate(test_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        adv_images = attack.perturb(images, labels)
        outputs = model(adv_images)
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
    print 'Accuracy = {}%'.format(float(correct) * 100 / 10000)
    print 64 * '-'


def evaluate_ifgsm(model, loss, eps, nb_iter, eps_iter, clip_min, clip_max, test_loader):
    attack = IterativeGradientAttack(model, loss, eps=eps, clip_min=clip_min, clip_max=clip_max,\
        steps=nb_iter)
    correct = 0
    print 'Evaluating on I-FGSM attack: eps = {}, eps_iter = {}, nb_iter = {}'.format(eps, eps_iter, nb_iter)
    for i, (images, labels) in enumerate(test_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        adv_images = attack.perturb(images, labels)
        outputs = model(adv_images)
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
    print 'Accuracy = {}%'.format(float(correct) * 100 / 10000)
    print 64 * '-'


def evaluate_pgd(model, loss, eps, nb_iter, eps_iter, clip_min, clip_max, test_loader):
    attack = ProjectedGradientDescent(model, loss=loss, eps=eps, eps_iter=eps_iter, steps=nb_iter,\
        clip_min=clip_min, clip_max=clip_max)
    correct = 0
    print 'Evaluating on PGD attack: eps = {}, eps_iter = {}, nb_iter = {}'.format(eps, eps_iter, nb_iter)
    for i, (images, labels) in enumerate(test_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        adv_images = attack.perturb(images, labels)
        outputs = model(adv_images)
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
    print 'Accuracy = {}%'.format(float(correct) * 100 / 10000)
    print 64 * '-'

def advertorch_evaluate_fgsm(model, loss, eps, clip_min, clip_max, test_loader):
    attack = GradientSignAttack(model, loss_fn=loss, eps=eps, \
        targeted=True, clip_min=clip_min, clip_max=clip_max)
    correct = 0
    print 'Evaluating on FGSM attack:eps = {}'.format(eps)
    for i, (images, labels) in enumerate(test_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        adv_images = attack(images, labels)
        outputs = model(adv_images)
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
    print 'Accuracy = {}%'.format(float(correct) * 100 / 10000)
    print 64 * '-'


def evaluate_ifgsm(model, loss, eps, nb_iter, eps_iter, clip_min, clip_max, test_loader):
    attack = LinfBasicIterativeAttack(model, loss_fn=loss, eps=eps, \
        targeted=True, clip_min=clip_min, clip_max=clip_max, \
        nb_iter=nb_iter, eps_iter=eps_iter)
    correct = 0
    print 'Evaluating on I-FGSM attack: eps = {}, eps_iter = {}, nb_iter = {}'.format(eps, eps_iter, nb_iter)
    for i, (images, labels) in enumerate(test_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        adv_images = attack(images, labels)
        outputs = model(adv_images)
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
    print 'Accuracy = {}%'.format(float(correct) * 100 / 10000)
    print 64 * '-'


def evaluate_pgd(model, loss, eps, nb_iter, eps_iter, clip_min, clip_max, test_loader):
    attack = LinfPGDAttack(model, loss_fn=loss, eps=eps, \
        targeted=True, clip_min=clip_min, clip_max=clip_max, \
        nb_iter=nb_iter, eps_iter=eps_iter)
    correct = 0
    print 'Evaluating on PGD attack: eps = {}, eps_iter = {}, nb_iter = {}'.format(eps, eps_iter, nb_iter)
    for i, (images, labels) in enumerate(test_loader):
        images, labels = images.to('cuda'), labels.to('cuda')
        adv_images = attack(images, labels)
        outputs = model(adv_images)
        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
    print 'Accuracy = {}%'.format(float(correct) * 100 / 10000)
    print 64 * '-'
"""
