import matplotlib.pyplot as plt
from matplotlib import rc
ATTACK = 'pgd'
MODEL = 'A'
NORMAL_FILE = 'eps_vs_{}_model_{}.txt'
ADVERTORCH_FILE = 'eps_vs_{}_model_{}_advertorch.txt'
mpl_font_config = {
    'family': 'sans-serif',
    'sans-serif' : ['Helvetica']
}
rc('font', **mpl_font_config)

with open(NORMAL_FILE.format(ATTACK, MODEL)) as f1, \
     open(ADVERTORCH_FILE.format(ATTACK, MODEL)) as f2:
    epsilons = []
    normal_losses = []
    advertorch_losses = []
    normal_accs = []
    advertorch_accs = []
    lines1 = [line.split(',') for line in f1]
    lines2 = [line.split(',') for line in f2]
    for _, [epsilon, acc, loss] in enumerate(lines1):
        epsilons.append(float(epsilon))
        normal_losses.append(float(loss))
        normal_accs.append(float(acc) * 100.0)
    for _, [_, acc, loss] in enumerate(lines2):
        advertorch_losses.append(float(loss))
        advertorch_accs.append(float(acc) * 100.0)

fig, ax = plt.subplots(2, 2)

# Left column is our
ax[0][0].plot(epsilons, normal_losses, 'r*-')
ax[0][0].set_xlabel('\epsilon')
ax[0][0].set_ylabel('Loss')
ax[0][0].set_title('Our implementation')

ax[1][0].plot(epsilons, normal_accs, 'go-')
ax[1][0].set_xlabel('Epsilon')
ax[1][0].set_ylabel('Accuracy (%)')
# Right column is advertorch
ax[0][1].plot(epsilons, advertorch_losses, 'r*-')
ax[0][1].set_xlabel('Epsilon')
ax[0][1].set_ylabel('Loss')
ax[0][1].set_title('AdverTorch')
ax[1][1].plot(epsilons, advertorch_accs, 'go-')
ax[1][1].set_xlabel('Epsilon')
ax[1][1].set_ylabel('Accuracy')

plt.show()
