import argparse
from model import ModelA, ModelB, ModelC, ModelD, MNIST_Network
from train import normal, fgsm_adv, ifgsm_adv, pgd_adv
from eval_model import evaluate, evaluate_grey
def get_model(modelname):
    if(modelname == 'A'):
        return ModelA()
    elif(modelname == 'B'):
        return ModelB()
    elif(modelname == 'C'):
        return ModelC()
    elif(modelname == 'D'):
        return ModelD()
    elif(modelname == 'mnist'):
        return MNIST_Network()

def main(mode, train, model, epochs, lr, batch_size, savedir, eval_model_cat, config):
    if(mode == 'train'):
        name = model
        train_model = get_model(name)
        if(train == 'normal'):
            normal(train_model, name, epochs, lr, batch_size, savedir)
        elif(train == 'fgsm_adv'):
            fgsm_adv(train_model, name, epochs, lr, batch_size, savedir)
        elif(train == 'ifgsm_adv'):
            ifgsm_adv(train_model, name, epochs, lr, batch_size, savedir)
        elif(train == 'pgd_adv'):
            pgd_adv(train_model, name, epochs, lr, batch_size, savedir)
    elif(mode == 'eval'):
        name = model
        eval_model = get_model(name)
        if eval_model_cat.endswith('_grey'):
            evaluate_grey(eval_model, name, savedir, eval_model_cat[:-len('_grey')], batch_size, config)
        else:
            evaluate(eval_model, name, savedir, eval_model_cat, batch_size, config)
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser('main.py')
    parser.add_argument('--mode', type=str, default='train', \
        help='Operating mode [train/eval], default: train')
    parser.add_argument('--train', type=str, default='normal', \
        help='Training mode [normal/fgsm_adv/ifgsm_adv/pgd_adv], default: normal')
    parser.add_argument('--model', type=str, default='A', \
        help='Choose a model for training or evaluating[A/B/C/D], default: A')
    parser.add_argument('--epochs', type=int, default=6, \
        help='Number of epochs for training, default: 6')
    parser.add_argument('--lr', type=float, default=0.001, \
        help='Learning rate for training, default: 0.001')
    parser.add_argument('--batch_size', type=int, default=64, \
        help='Batch size for training, default: 64')
    parser.add_argument('--savedir', type=str, default='./saved_models/', \
        help='Directory for saving models, default: ./saved_models/')
    parser.add_argument('--eval_model_cat', type=str, \
        default='normal', \
        help='Training type of the model to be evaluated[normal/fgsm_adv/ifgsm_adv/pgd_adv], default: normal')
    parser.add_argument('--config', type=str, default='./configs/config.json', \
        help='Path to config file for a number of hyper parameters, default: ./configs/config.json')
    args = parser.parse_args()
    main(**vars(args))
