#!/bin/bash

GPU=3
EPOCHS=20
LEARNING_RATE=1e-2
BATCH_SIZE=64
SAVEDIR='./saved_models/'
MODE='eval'
MODEL='mnist'
TRAIN='fgsm_adv'
EVALMODELCAT='fgsm_adv'
CUDA_VISIBLE_DEVICES=$GPU python main.py --mode $MODE --train $TRAIN \
 --model $MODEL	--epochs $EPOCHS --lr $LEARNING_RATE --batch_size $BATCH_SIZE \
 --savedir $SAVEDIR --eval_model_cat $EVALMODELCAT
