import numpy as np
import torch
from torch.autograd import Variable
from torch.autograd.gradcheck import zero_gradients

class ProjectedGradientDescent():
    def __init__(self, model, loss, eps=0.3, eps_iter=0.01, steps=40, \
            clip_min=0., clip_max=1.):
        assert model is not None
        assert loss is not None
        assert model.training is not True
        self.model = model
        self.loss = loss
        self.eps = eps
        self.eps_iter = eps_iter
        self.steps = steps
        self.clip_min = clip_min
        self.clip_max = clip_max
    def __call__(self, x, y):
        return self.perturb(x, y)
    def perturb(self, x, y):
        assert x is not None
        assert y is not None
        x, y = x.to('cuda'), y.to('cuda')
        B, C, H, W = x.size()

        # Start from a random point in space
        noise = torch.cuda.FloatTensor(np.random.uniform(-self.eps, self.eps, \
                    (B, C, H, W)))
        noise = torch.clamp(noise, -self.eps, self.eps)
        for step in range(self.steps):
            img = x + noise
            img = Variable(img, requires_grad=True)
            img.to('cuda')

            zero_gradients(img)
            
            out = self.model(img)
            cost = self.loss(out, y)

            cost.backward()

            perturbation = self.eps_iter * torch.sign(img.grad.data)
            adv_img = img.data + perturbation.to('cuda')

            img = torch.clamp(adv_img, self.clip_min, self.clip_max)
            noise = img - x
            noise = torch.clamp(noise, -self.eps, self.eps)
        img = x + noise
        return img
