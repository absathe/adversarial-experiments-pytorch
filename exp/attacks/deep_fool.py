import copy
import numpy as np
import torch
from torch.autograd import Variable
from torch.autograd.gradcheck import zero_gradients

class DeepFool():
    def __init__(self, model, nb_candidate=10, overshoot=0.02, max_iter=10, clip_min=0, clip_max=1, max_l2=1):
        assert model.training is not True
        self.model = model
        self.nb_candidate = nb_candidate
        self.overshoot = overshoot
        self.max_iter = max_iter
        self.clip_min = clip_min
        self.clip_max = clip_max
        self.max_l2 = max_l2

    def __call__(self, sample):
        return self.perturb(sample)

    def model_argmax(self, model, x):
        zero_gradients(x)
        x_in = Variable(torch.FloatTensor(x).to('cuda'))
        out = model(x_in)
        idx = out.max(1)[1]
        idx = idx.data.to('cpu').numpy()
        return idx
    
    def model_get_grad_predval(self, model, x):
        x_in = Variable(torch.FloatTensor(x).to('cuda'), requires_grad=True)
        zero_gradients(x_in)
        out = model(x_in)
        # sort the score in descending order for each samples
        out_s, idx = torch.sort(out, dim=1, descending=True)
        # get gradients
        B,C,H,W = x_in.size()
        classes = out.size()[1]
        gradients = np.zeros([B,classes,C,H,W])
        for i in range(out.size(1)):
            zero_gradients(x_in)
            # https://discuss.pytorch.org/t/whats-difference-between-retain-graph-and-retain-variables/12114/2
            out_s[:,i].backward(torch.ones(B).to('cuda'),retain_graph=True)
            grad = x_in.grad
            grad = grad.view(B,1,C,H,W)
            grad = grad.data.to('cpu').numpy()
            gradients[:,i:i+1,:,:,:] = grad
        predval = out_s.data.to('cpu').numpy()
        return predval, gradients

    def perturb(self, sample):
        sample = sample.numpy()
        adv_x = copy.copy(sample)

        # Initialize the loop variables
        iteration = 0
        current = self.model_argmax(self.model, adv_x)
        if current.shape == ():
            current = np.array([current])

        # Same shape as original image
        w = np.squeeze(np.zeros(sample.shape[1:]))
        r_tot = np.zeros(sample.shape)
        original = current # Using the original label as referencce

        # Repeat till exhausted `max_iter` or misclassified
        while (np.any(current == original) and iteration < self.max_iter):
            predictions_val, gradients = self.model_get_grad_predval(self.model, adv_x)
            for idx in range(sample.shape[0]):
                pert = np.inf
                if current[idx] != original[idx]:
                    continue
                for k in range(1, self.nb_candidate):
                    w_k = gradients[idx, k, ...] - gradients[idx, 0, ...]
                    f_k = predictions_val[idx, k] - predictions_val[idx, 0]
                    # Add a small value (0.00001) to prevent f_k = 0
                    pert_k = (abs(f_k) + 0.00001) / np.linalg.norm(w_k.flatten())
                    if pert_k < pert:
                        pert = pert_k
                        w = w_k
                r_i = pert * w / np.linalg.norm(w)
                r_tot[idx, ...] = r_tot[idx, ...] + r_i
            # modified section
            r_tot = torch.FloatTensor(r_tot).to('cuda')
            adv_x = r_tot + torch.FloatTensor(sample).to('cuda')
            adv_x = adv_x.to('cpu').numpy()
            r_tot = r_tot.to('cpu').numpy()

            # adv_x = np.clip(mask*r_tot + sample, clip_min, clip_max)

            current = self.model_argmax(self.model, adv_x)
            if current.shape == ():
                current = np.array([current])
            # Update loop variable
            iteration += 1
        adv_x = np.clip((1+self.overshoot)*r_tot + sample, self.clip_min, self.clip_max)
        adv_x = torch.FloatTensor(adv_x)
        return adv_x
